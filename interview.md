# Interview
## Best practices
### Energizer
* To get everyone comfortable with the situation:
	* What did you have for breakfast?
### An approachable voice
* Speak like a an agreeable waiter would speak to a customer
### Don't be verbose
* Keep questions simple yet to the point

## Structure
### Introduction (~1 minute)
(Format, interviewee introduction, and why that person)
* Format
	* This is an interview format we're testing that will last about 10 to 15 minutes
* Interviewee introduction
	* I am with Laury who is Shodo's chief of communication. Hi X, What do you do at Shodo?
* Why that person
	* You recently asked a company to build a website for Shodo.
		* Therefore you have been on the customer side of an IT project
		* As an engineer myself, I am curious about your feedback
### Main Content (~10 minutes)
#### About: the project and the project team
* Broadly speaking, what was the project about and how long has it been lasting?
* How many people in the team?
* How long did you want to website to remain in this new state?
#### About: the way you interacted with the team & product
* Description:
	* Your interaction with the team:
		* did the team introduce to you how they worked?
		* who did you communicate with?
		* how often did you communication?
		* in which format?
	* Your interaction with the state of the product
* Feedback:
	* Were you satisfied with the way you interacted with the team?
	* Were you satisfied with the way you interacted with the state of the product?
#### About: the way you communicated your need to the team
* Description:
	* how did you communicate your need?
		* Who initiated the information flow?
		* Did the team help you in your decisions?
* Feedback:
	* Were you satisfied with the way they were understood?
#### Expectations vs Reality
* Any expectations you had to lower because of technical constraints?
* Things more complex or easier than you thought initially?
* Good/Bad surprises?
* How long you thought the project would last vs how long it actually lasted
### Closing thoughts (~undefined)
* Any information you want to add?
* The one word worth remembering from your experience?
* Thanks and 'til next time!
